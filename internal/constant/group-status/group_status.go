package groupstatus

const (
	Pending = iota
	Active
	Deleted
)
