package pkg

import (
	"context"
	"time"

	"demo/pkg/l"

	"go.uber.org/zap/zapcore"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// LogUnaryServerInterceptor returns middleware for logging with zap
func LogUnaryServerInterceptor(logger l.Logger, excepts ...error) grpc.UnaryServerInterceptor {
	m := make(map[error]struct{})
	for _, err := range excepts {
		m[err] = struct{}{}
	}
	getLogFn := func(v interface{}) (lg func(msg string, fields ...zapcore.Field)) {
		if err, ok := v.(error); ok {
			defer func() {
				if e := recover(); e != nil {
					lg = logger.Error
				}
			}()
			if _, isExcept := m[err]; isExcept {
				return logger.Warn
			}
			return logger.Error
		}
		return logger.Debug
	}

	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
		start := time.Now()
		defer func() {
			t := time.Now().Sub(start)

			e := recover()
			if e != nil {
				logger.S.Error("Panic (Recovered)", l.String("FullMethod", info.FullMethod),
					l.Error(err), l.Stack())
				err = grpc.Errorf(codes.Internal, "Internal Error (%v)", e)
			}

			if err == nil {
				logFn := getLogFn(resp)
				logFn(info.FullMethod, l.Duration("t", t),
					l.Interface("→ ", req), l.Interface("⇐ ", resp))
				return
			}

			logFn := getLogFn(err)

			// Append details
			if errorStatus, ok := status.FromError(err); ok {
				logFn(info.FullMethod, l.Duration("t", t),
					l.Interface("→ ", req), l.Stringer("⇐ ⇐ ERROR", errorStatus.Proto()))
			} else {
				logFn(info.FullMethod, l.Duration("t", t),
					l.Interface("→ ", req), l.String("⇐ ERROR", err.Error()), l.Error(err))
			}
		}()
		return handler(ctx, req)
	}
}
