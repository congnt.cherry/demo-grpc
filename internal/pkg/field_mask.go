package pkg

import (
	"fmt"
	"log"
	"reflect"
	"regexp"
	"strings"

	"github.com/gogo/protobuf/types"
)

var uppercaseAcronym = map[string]bool{
	"ID": true,
}

var numberSequence = regexp.MustCompile(`([a-zA-Z])(\d+)([a-zA-Z]?)`)
var numberReplacement = []byte(`$1 $2 $3`)

func addWordBoundariesToNumbers(s string) string {
	b := []byte(s)
	b = numberSequence.ReplaceAll(b, numberReplacement)
	return string(b)
}

// Converts a string to CamelCase
func toCamelInitCase(s string, initCase bool) string {
	s = addWordBoundariesToNumbers(s)
	s = strings.Trim(s, " ")
	n := ""
	capNext := initCase
	for _, v := range s {
		if v >= 'A' && v <= 'Z' {
			n += string(v)
		}
		if v >= '0' && v <= '9' {
			n += string(v)
		}
		if v >= 'a' && v <= 'z' {
			if capNext {
				n += strings.ToUpper(string(v))
			} else {
				n += string(v)
			}
		}
		if v == '_' || v == ' ' || v == '-' || v == '.' {
			capNext = true
		} else {
			capNext = false
		}
	}
	return n
}

// ToCamel converts a string to CamelCase
func ToCamel(s string) string {
	if uppercaseAcronym[s] {
		s = strings.ToLower(s)
	}
	return toCamelInitCase(s, true)
}

func ApplyFieldMask(patchee, patcher interface{}, mask *types.FieldMask) {
	if mask == nil {
		return
	}

	for _, path := range mask.GetPaths() {
		val := GetMaskedField(patcher, path)
		fmt.Println("==>", path, val, val.Type().Name())
		switch val.Type().Name() {
		case "Group_GroupPrivacy", "Group_GroupDiscoverability":
			val = reflect.ValueOf(val.Int())
		}
		if val.IsValid() {
			fmt.Println("	==> isvalid")
			v := reflect.ValueOf(patchee)
			s := reflect.ValueOf(path)
			v.SetMapIndex(s, val)
		}
	}
}

func GetMaskedField(obj interface{}, path string) (val reflect.Value) {
	defer func() {
		if r := recover(); r != nil {
			log.Printf("failed to get field:\npath: %q\nobj: %#v\nerr: %v", path, obj, r)
			val = reflect.Value{}
		}
	}()

	v := reflect.ValueOf(obj)
	if len(path) == 0 {
		return v
	}

	for _, s := range strings.Split(path, ".") {
		if v.Kind() == reflect.Ptr {
			v = reflect.Indirect(v)
		}
		v = v.FieldByName(ToCamel(s))
	}

	return v
}
