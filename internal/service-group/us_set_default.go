package groupservice

import (
	"context"

	serrors "demo/internal/errors"
	"demo/internal/model/aggregate"
	"demo/internal/service-group/dto"
	"demo/pkg/errors"
	"demo/pkg/l"
)

func (s service) SetDefault(ctx context.Context, req dto.SetDefaultRequest) (*aggregate.Group, errors.IError) {
	if err := req.Validate(); err != nil {
		return nil, serrors.ErrInvalidArgument(err).Log("invalid arg", l.Interface("req", req))
	}

	group, err := s.groupRepo.SetDefault(ctx, req.ID, req.IsDefault)
	if err != nil {
		return nil, serrors.ErrInternal(err)
	}

	rs := &aggregate.Group{}
	rs.FromEntityGroup(group)

	return rs, nil
}
