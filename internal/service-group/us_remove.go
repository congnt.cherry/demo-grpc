package groupservice

import (
	"context"

	serrors "demo/internal/errors"
	"demo/internal/service-group/dto"
	"demo/pkg/errors"
	"demo/pkg/l"
)

func (s service) Remove(ctx context.Context, req dto.RemoveRequest) errors.IError {
	if err := req.Validate(); err != nil {
		return serrors.ErrInvalidArgument(err).Log("invalid arg", l.Interface("req", req))
	}

	if err := s.groupRepo.Remove(ctx, req.ID); err != nil {
		return serrors.ErrInternal(err)
	}

	return nil
}
