package groupservice

import (
	"context"
	"demo/internal/model/aggregate"

	"demo/internal/service-group/dto"
	"demo/pkg/errors"
)

// IService ...
type IService interface {
	Create(ctx context.Context, req dto.CreateRequest) (*aggregate.Group, errors.IError)
	Update(ctx context.Context, req dto.UpdateRequest) (*aggregate.Group, errors.IError)
	Remove(ctx context.Context, req dto.RemoveRequest) errors.IError
	FindOne(ctx context.Context, req dto.FindOneRequest) (*aggregate.Group, errors.IError)
	FindAll(ctx context.Context, req dto.FindAllRequest) ([]*aggregate.Group, errors.IError)
	SetDefault(ctx context.Context, req dto.SetDefaultRequest) (*aggregate.Group, errors.IError)
}
