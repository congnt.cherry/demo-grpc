package dto

import (
	"fmt"

	"gopkg.in/go-playground/validator.v9"
)

type UpdateRequest struct {
	ID   string                 `json:"id" validate:"required"`
	Data map[string]interface{} `json:"data" validate:"required"`
}

func (s *UpdateRequest) Validate() error {
	validate := validator.New()
	err := validate.Struct(s)
	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			return fmt.Errorf("[ValidateUpdateRequest] Invalid request in field [%+v], tag [%+v], value [%+v]", err.StructNamespace(), err.Tag(), err.Value())
		}
	}

	return nil
}
