package dto

import (
	"fmt"

	"gopkg.in/go-playground/validator.v9"
)

type CreateRequest struct {
	Name            string `json:"name" validate:"required,gte=1,lte=25"`
	Description     string `json:"description"`
	Privacy         int    `json:"privacy" validate:"required"`
	Discoverability int    `json:"point"`
	Cover           string `json:"cover"`
	Alias           string `json:"alias"`
	BackgroundImage string `json:"background_image"`
	CreatorID       string `json:"creator_id"`
}

func (s *CreateRequest) Validate() error {
	validate := validator.New()
	err := validate.Struct(s)
	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			return fmt.Errorf("[ValidateCreateBadgeRequest] Invalid request in field [%+v], tag [%+v], value [%+v]", err.StructNamespace(), err.Tag(), err.Param())
		}
	}

	return nil
}
