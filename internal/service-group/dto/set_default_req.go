package dto

import (
	"fmt"

	"gopkg.in/go-playground/validator.v9"
)

type SetDefaultRequest struct {
	ID        string `json:"id" validate:"required"`
	IsDefault bool   `json:"is_default"`
}

func (s *SetDefaultRequest) Validate() error {
	validate := validator.New()
	err := validate.Struct(s)
	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			return fmt.Errorf("[ValidateSetDefaultRequest] Invalid request in field [%+v], tag [%+v], value [%+v]", err.StructNamespace(), err.Tag(), err.Value())
		}
	}

	return nil
}
