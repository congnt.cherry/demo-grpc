package groupservice

import (
	"context"

	groupstatus "demo/internal/constant/group-status"
	serrors "demo/internal/errors"
	"demo/internal/model/aggregate"
	"demo/internal/model/entity"
	"demo/internal/service-group/dto"
	"demo/pkg/errors"
	"demo/pkg/l"
)

// Create ...
func (s service) Create(ctx context.Context, req dto.CreateRequest) (*aggregate.Group, errors.IError) {
	if err := req.Validate(); err != nil {
		return nil, serrors.ErrInvalidArgument(err).Log("invalid arg", l.Interface("req", req))
	}

	group := &entity.Group{
		Name:            req.Name,
		Description:     req.Description,
		CreatorID:       req.CreatorID,
		Cover:           req.Cover,
		BackgroundImage: req.BackgroundImage,
		Privacy:         req.Privacy,
		Discovery:       req.Discoverability,
		Alias:           req.Alias,
		IsDefault:       false,
		Status:          groupstatus.Active,
	}

	if err := s.groupRepo.Save(ctx, group); err != nil {
		return nil, serrors.ErrInternal(err).Log("failed to create group", l.Interface("obj", group))
	}

	rs := &aggregate.Group{}
	rs.FromEntityGroup(group)

	return rs, nil
}
