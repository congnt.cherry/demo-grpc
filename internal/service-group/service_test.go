package groupservice

import (
	"context"
	"reflect"
	"testing"

	"demo/internal/model/aggregate"
	"demo/internal/model/entity"
	"demo/internal/repository/group"
	"demo/internal/service-group/dto"
	"demo/pkg/container"
	"demo/pkg/errors"
	"demo/pkg/l"
	. "github.com/smartystreets/goconvey/convey"
)

func TestNew(t *testing.T) {
	container.Register(func() l.Logger {
		return l.New()
	})
	container.Register(func() group.IRepository {
		mockedIRepository := &group.IRepositoryMock{
			GetActiveByIDFunc: func(ctx context.Context, id string) (*entity.Group, error) {
				panic("mock out the GetActiveByID method")
			},
			ListActiveFunc: func(ctx context.Context, limit int64, offset int64) ([]*entity.Group, error) {
				panic("mock out the ListActive method")
			},
			RemoveFunc: func(ctx context.Context, id string) error {
				panic("mock out the Remove method")
			},
			SaveFunc: func(ctx context.Context, obj *entity.Group) error {
				panic("mock out the Save method")
			},
			SetDefaultFunc: func(ctx context.Context, id string, isDefault bool) (*entity.Group, error) {
				panic("mock out the SetDefault method")
			},
			UpdateMapFunc: func(ctx context.Context, id string, obj map[string]interface{}) (*entity.Group, error) {
				panic("mock out the UpdateMap method")
			},
		}
		return mockedIRepository
	})

	got := New()

	if !reflect.TypeOf(got).Elem().Implements(reflect.TypeOf(new(IService)).Elem()) {
		t.Error("Must satisfy IService interface")
	}
}

func Test_service_Create(t *testing.T) {

	Convey("Test_service_Create", t, func() {
		container.Register(func() l.Logger {
			return l.New()
		})
		container.Register(func() group.IRepository {
			mockedIRepository := &group.IRepositoryMock{
				GetActiveByIDFunc: func(ctx context.Context, id string) (*entity.Group, error) {
					panic("mock out the GetActiveByID method")
				},
				ListActiveFunc: func(ctx context.Context, limit int64, offset int64) ([]*entity.Group, error) {
					panic("mock out the ListActive method")
				},
				RemoveFunc: func(ctx context.Context, id string) error {
					panic("mock out the Remove method")
				},
				SaveFunc: func(ctx context.Context, obj *entity.Group) error {
					obj.BeforeCreate()
					return nil
				},
				SetDefaultFunc: func(ctx context.Context, id string, isDefault bool) (*entity.Group, error) {
					panic("mock out the SetDefault method")
				},
				UpdateMapFunc: func(ctx context.Context, id string, obj map[string]interface{}) (*entity.Group, error) {
					panic("mock out the UpdateMap method")
				},
			}
			return mockedIRepository
		})

		s := New()

		Convey("When create", func() {
			ctx := context.TODO()
			rs, err := s.Create(ctx, dto.CreateRequest{
				Name:            "name test",
				Description:     "this is a description",
				CreatorID:       "user1",
				Cover:           "",
				BackgroundImage: "",
				Privacy:         1,
				Discoverability: 1,
				Alias:           "test",
			})
			Convey("Received nil error", func() {
				So(err, ShouldBeNil)
			})
			Convey("Received result with not nil", func() {
				So(rs, ShouldNotBeNil)
			})

		})

	})
}

func Test_service_FindAll(t *testing.T) {
	type fields struct {
		ll        l.Logger
		groupRepo group.IRepository
	}
	type args struct {
		ctx context.Context
		req dto.FindAllRequest
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   []*aggregate.Group
		want1  errors.IError
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service{
				ll:        tt.fields.ll,
				groupRepo: tt.fields.groupRepo,
			}
			got, got1 := s.FindAll(tt.args.ctx, tt.args.req)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FindAll() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("FindAll() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func Test_service_FindOne(t *testing.T) {
	type fields struct {
		ll        l.Logger
		groupRepo group.IRepository
	}
	type args struct {
		ctx context.Context
		req dto.FindOneRequest
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *aggregate.Group
		want1  errors.IError
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service{
				ll:        tt.fields.ll,
				groupRepo: tt.fields.groupRepo,
			}
			got, got1 := s.FindOne(tt.args.ctx, tt.args.req)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FindOne() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("FindOne() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func Test_service_Remove(t *testing.T) {
	type fields struct {
		ll        l.Logger
		groupRepo group.IRepository
	}
	type args struct {
		ctx context.Context
		req dto.RemoveRequest
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   errors.IError
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service{
				ll:        tt.fields.ll,
				groupRepo: tt.fields.groupRepo,
			}
			if got := s.Remove(tt.args.ctx, tt.args.req); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Remove() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_service_SetDefault(t *testing.T) {
	type fields struct {
		ll        l.Logger
		groupRepo group.IRepository
	}
	type args struct {
		ctx context.Context
		req dto.SetDefaultRequest
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *aggregate.Group
		want1  errors.IError
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service{
				ll:        tt.fields.ll,
				groupRepo: tt.fields.groupRepo,
			}
			got, got1 := s.SetDefault(tt.args.ctx, tt.args.req)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("SetDefault() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("SetDefault() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func Test_service_Update(t *testing.T) {
	type fields struct {
		ll        l.Logger
		groupRepo group.IRepository
	}
	type args struct {
		ctx context.Context
		req dto.UpdateRequest
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *aggregate.Group
		want1  errors.IError
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service{
				ll:        tt.fields.ll,
				groupRepo: tt.fields.groupRepo,
			}
			got, got1 := s.Update(tt.args.ctx, tt.args.req)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Update() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("Update() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
