package groupservice

import (
	"context"

	serrors "demo/internal/errors"
	"demo/internal/model/aggregate"
	"demo/internal/service-group/dto"
	groupstore "demo/internal/store/mysql/group"
	"demo/pkg/errors"
)

const (
	defaultLimit int64 = 20
)

func (s service) FindOne(ctx context.Context, req dto.FindOneRequest) (*aggregate.Group, errors.IError) {
	if err := req.Validate(); err != nil {
		return nil, serrors.ErrInvalidArgument(err)
	}
	group, err := s.groupRepo.GetActiveByID(ctx, req.ID)
	if err != nil {
		if err == groupstore.ErrNotFoundGroup {
			return nil, serrors.ErrNotFoundGroup(err)
		}
		return nil, serrors.ErrInternal(err)
	}

	rs := &aggregate.Group{}
	rs.FromEntityGroup(group)

	return rs, nil
}

func (s service) FindAll(ctx context.Context, req dto.FindAllRequest) ([]*aggregate.Group, errors.IError) {
	limit := defaultLimit
	if req.Limit <= 0 {
		limit = req.Limit
	}
	offset := req.Offset
	if offset < 0 {
		offset = 0
	}

	groups, err := s.groupRepo.ListActive(ctx, limit, offset)
	if err != nil {
		return nil, serrors.ErrInternal(err)
	}

	rs := &aggregate.ListGroupsAggregate{}
	rs.FromEntities(groups)

	return *rs, nil
}
