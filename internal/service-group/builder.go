package groupservice

import (
	"demo/internal/repository/group"
	"demo/pkg/container"
	"demo/pkg/l"
)

type service struct {
	ll        l.Logger          `di:"inject"`
	groupRepo group.IRepository `di:"inject"`
}

var _ IService = &service{}

func New() *service {
	obj := &service{}
	container.Fill(obj)
	return obj
}
