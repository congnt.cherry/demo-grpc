package groupservice

import (
	"context"

	serrors "demo/internal/errors"
	"demo/internal/model/aggregate"
	"demo/internal/service-group/dto"
	"demo/pkg/errors"
	"demo/pkg/l"
)

func (s service) Update(ctx context.Context, req dto.UpdateRequest) (*aggregate.Group, errors.IError) {
	if err := req.Validate(); err != nil {
		return nil, serrors.ErrInvalidArgument(err).Log("invalid arg", l.Interface("req", req))
	}

	group, err := s.groupRepo.UpdateMap(ctx, req.ID, req.Data)
	if err != nil {
		s.ll.Error("failed to update ", l.Error(err))
		return nil, serrors.ErrInternal(err)
	}

	rs := &aggregate.Group{}
	rs.FromEntityGroup(group)

	return rs, nil
}
