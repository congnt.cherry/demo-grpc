package group

import (
	"context"

	"demo/internal/model/entity"
)

//go:generate thor -out ../../store/mysql/group/group.go -fmt goimports -d mysql -model entity.Group -pkg groupstore . IRepository:groupRepoImpl
//go:generate thor -out ../../store/mysql/group/group_impl.go -fmt goimports -d impl -model entity.Group -pkg groupstore . IRepository:groupRepoImpl

type IRepository interface {
	Save(ctx context.Context, obj *entity.Group) error
	Remove(ctx context.Context, id string) error
	GetActiveByID(ctx context.Context, id string) (*entity.Group, error)
	ListActive(ctx context.Context, limit int64, offset int64) ([]*entity.Group, error)
	UpdateMap(ctx context.Context, id string, obj map[string]interface{}) (*entity.Group, error)
	SetDefault(ctx context.Context, id string, isDefault bool) (*entity.Group, error)
}
