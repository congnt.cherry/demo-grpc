package groupstore

import (
	"context"
	"testing"

	"demo/config"
	groupstatus "demo/internal/constant/group-status"
	"demo/internal/model/entity"
	"demo/pkg/l"
	"demo/pkg/mysql"

	. "github.com/smartystreets/goconvey/convey"
)

func Test_groupRepoImpl_Save(t *testing.T) {
	Convey("Test_groupRepoImpl_Save", t, func() {
		ll := l.New()
		cfg := config.Load(ll, "../../../../")
		db := mysql.MustConnectDB(cfg.Mysql)
		So(db, ShouldNotBeNil)
		defer db.Close()

		var groupID string
		groupStore := NewGroupRepoImpl(db)
		So(groupStore, ShouldNotBeNil)
		Convey("When saving the group with valid info", func() {
			// Setup the cleaner
			//cleaner := DeleteCreatedEntities(db)
			//defer cleaner()
			ctx := context.TODO()
			groupObj := &entity.Group{
				Name:            "name test",
				Description:     "this is a description",
				CreatorID:       "user1",
				Cover:           "",
				BackgroundImage: "",
				Privacy:         0,
				Discovery:       0,
				Alias:           "test",
				Status:          groupstatus.Active,
			}
			err := groupStore.Save(ctx, groupObj)
			Convey("Received nil error", func() {
				So(err, ShouldBeNil)
			})

			groupID = groupObj.ID
			Convey("The group id should not be empty", func() {
				So(groupID, ShouldNotBeEmpty)
			})

			Convey("When get active group", func() {
				// Setup the cleaner
				//cleaner := DeleteCreatedEntities(db)
				//defer cleaner()
				ctx := context.TODO()
				group, err := groupStore.GetActiveByID(ctx, groupID)
				Convey("Received nil error", func() {
					So(err, ShouldBeNil)
				})
				Convey("Received group object not nil", func() {
					So(group, ShouldNotBeNil)
				})
				Convey("Received group with correct id", func() {
					So(groupID, ShouldEqual, group.ID)
				})
			})

		})

	})
}
