package serrors

import (
	"demo/pkg/errors"
)

func ErrUnauthenticated(err error) *errors.APIError {
	return errors.Error(errors.Unauthenticated, "Unauthenticated", err)
}

func ErrInternal(err error) *errors.APIError {
	return errors.Error(errors.Internal, "Internal error", err)
}

func ErrFailedPrecondition(err error) *errors.APIError {
	return errors.Error(errors.FailedPrecondition, "FailedPrecondition", err)
}

func ErrInvalidArgument(err error) *errors.APIError {
	return errors.Error(errors.InvalidArgument, "InvalidArgument", err)
}

func ErrNotFoundGroup(err error) *errors.APIError {
	return errors.Error(errors.NotFound, "Not found group", err)
}
