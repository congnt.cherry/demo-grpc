package entity

import (
	"github.com/rs/xid"
)

// Group ...
type Group struct {
	ID              string `json:"id" gorm:"column:id;primary_key"`
	Name            string `json:"name" gorm:"column:name"`
	Description     string `json:"description" gorm:"column:description"`
	CreatorID       string `json:"creator_id" gorm:"column:creator_id"`
	Cover           string `json:"cover" gorm:"column:cover"`
	BackgroundImage string `json:"background_image" gorm:"column:background_image"`
	Privacy         int    `json:"privacy" gorm:"column:privacy"`
	Discovery       int    `json:"discovery" gorm:"column:discovery"`

	CreatedAt int64  `json:"created_at" gorm:"column:created_at"`
	UpdatedAt int64  `json:"updated_at" gorm:"column:updated_at"`
	DeletedAt *int64 `json:"deleted_at" gorm:"column:deleted_at"`
	Alias     string `json:"alias" gorm:"column:alias"`
	IsDefault bool   `json:"is_default" gorm:"column:is_default"`
	Status    int    `json:"status" gorm:"column:status"`
}

func (m *Group) BeforeCreate() {
	if m.ID != "" {
		return
	}
	guid := xid.New()
	m.ID = guid.String()
	//m.CreatedAt = time.Now().
	//m.UpdatedAt = time.Now()
}

func (m *Group) BeforeUpdate() {
	//m.UpdatedAt = time.Now()
}

func (m Group) TableName() string {
	return "group"
}
