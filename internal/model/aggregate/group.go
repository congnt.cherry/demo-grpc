package aggregate

import (
	"demo/internal/model/entity"
)

// Group ...
type Group struct {
	ID              string `json:"id"`
	Name            string `json:"name"`
	Description     string `json:"description"`
	CreatorID       string `json:"creator_id"`
	Cover           string `json:"cover"`
	BackgroundImage string `json:"background_image"`
	Privacy         int    `json:"privacy"`
	Discovery       int    `json:"discovery"`

	CreatedAt int64  `json:"created_at"`
	UpdatedAt int64  `json:"updated_at"`
	DeletedAt *int64 `json:"deleted_at"`
	Alias     string `json:"alias"`
	IsDefault bool   `json:"is_default"`
	Status    int    `json:"status"`

	// may be have members of group
}

// FromEntityGroup ...
func (o *Group) FromEntityGroup(group *entity.Group) {
	o.ID = group.ID
	o.Name = group.Name
	o.Description = group.Description
	o.CreatorID = group.CreatorID
	o.Cover = group.Cover
	o.BackgroundImage = group.BackgroundImage
	o.Privacy = group.Privacy
	o.Discovery = group.Discovery

	o.CreatedAt = group.CreatedAt
	o.UpdatedAt = group.UpdatedAt
	o.DeletedAt = group.DeletedAt
	o.Alias = group.Alias
	o.IsDefault = group.IsDefault
	o.Status = group.Status
}

// ListGroupsAggregate ...
type ListGroupsAggregate []*Group

// FromEntities ...
func (o *ListGroupsAggregate) FromEntities(objs []*entity.Group) {
	for _, item := range objs {
		data := &Group{}
		data.FromEntityGroup(item)
		*o = append(*o, data)
	}
}
