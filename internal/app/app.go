package app

import (
	"context"
	"demo/internal/pkg"
	groupservice "demo/internal/service-group"
	"demo/internal/service-group/dto"
	"demo/pkg/container"
	"demo/pkg/l"
	"encoding/json"

	pbdemo "demo/pb"

	"github.com/gogo/protobuf/types"
)

type service struct {
	ll           l.Logger              `di:"inject"`
	groupService groupservice.IService `di:"inject"`
}

func (s service) NewGroup(ctx context.Context, request *pbdemo.NewGroupRequest) (*pbdemo.NewGroupResponse, error) {
	req := dto.CreateRequest{
		Name:            request.GetName(),
		Description:     request.GetDescription(),
		Privacy:         int(request.GetPrivacy()),
		Discoverability: int(request.GetDiscoverability()),
		Cover:           request.GetCover(),
		Alias:           request.GetAlias(),
		BackgroundImage: request.GetBackgroundImage(),
		CreatorID:       request.GetCreatorId(),
	}
	rs, err := s.groupService.Create(ctx, req)
	if err != nil {
		return nil, err
	}

	return &pbdemo.NewGroupResponse{
		Data: ToProtoGroup(rs),
	}, nil
}

func (s service) UpdateGroup(ctx context.Context, request *pbdemo.UpdateGroupRequest) (*pbdemo.UpdateGroupResponse, error) {
	final := make(map[string]interface{})

	if request.GetUpdateMask() != nil && len(request.GetUpdateMask().GetPaths()) != 0 {
		pkg.ApplyFieldMask(final, request.GetGroup(), request.GetUpdateMask())
	} else { // update all fields
		reqBytes, err := json.Marshal(request.GetGroup())
		if err != nil {
			return nil, err
		}
		if err := json.Unmarshal(reqBytes, &final); err != nil {
			return nil, err
		}
	}

	rs, err := s.groupService.Update(ctx, dto.UpdateRequest{
		ID:   request.GetId(),
		Data: final,
	})
	if err != nil {
		return nil, err
	}

	return &pbdemo.UpdateGroupResponse{
		Data: ToProtoGroup(rs),
	}, nil
}

func (s service) DeleteGroup(ctx context.Context, request *pbdemo.DeleteGroupRequest) (*types.Empty, error) {
	err := s.groupService.Remove(ctx, dto.RemoveRequest{
		ID: request.GetId(),
	})
	if err != nil {
		return nil, err
	}
	return &types.Empty{}, nil
}

func (s service) ListGroups(ctx context.Context, request *pbdemo.ListGroupsRequest) (*pbdemo.ListGroupsResponse, error) {
	groups, err := s.groupService.FindAll(ctx, dto.FindAllRequest{
		Limit:  request.GetLimit(),
		Offset: request.GetOffset(),
	})

	if err != nil {
		return nil, err
	}

	return &pbdemo.ListGroupsResponse{
		Data: ToProtoGroups(groups),
	}, nil
}

func (s service) SetGroupDefault(ctx context.Context, request *pbdemo.SetGroupDefaultRequest) (*pbdemo.SetGroupDefaultResponse, error) {
	group, err := s.groupService.SetDefault(ctx, dto.SetDefaultRequest{ID: request.GetId(), IsDefault: request.GetIsDefault() == 1})
	if err != nil {
		return nil, err
	}
	return &pbdemo.SetGroupDefaultResponse{Data: ToProtoGroup(group)}, nil
}

func (s service) GetGroup(ctx context.Context, request *pbdemo.GetGroupRequest) (*pbdemo.GetGroupResponse, error) {
	group, err := s.groupService.FindOne(ctx, dto.FindOneRequest{ID: request.GetId()})
	if err != nil {
		return nil, err
	}
	return &pbdemo.GetGroupResponse{Data: ToProtoGroup(group)}, nil
}

var _ pbdemo.DemoServer = (*service)(nil)

func New() *service {
	obj := &service{}
	container.Fill(obj)
	return obj
}
