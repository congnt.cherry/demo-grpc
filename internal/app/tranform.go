package app

import (
	"demo/internal/model/aggregate"
	pbdemo "demo/pb"
)

func ToProtoGroup(obj *aggregate.Group) *pbdemo.Group {
	return &pbdemo.Group{
		Id:              obj.ID,
		Name:            obj.Name,
		Description:     obj.Description,
		Privacy:         pbdemo.Group_GroupPrivacy(obj.Privacy),
		OwnerId:         obj.CreatorID,
		Cover:           obj.Cover,
		CreatedAt:       obj.CreatedAt,
		Discoverability: pbdemo.Group_GroupDiscoverability(obj.Discovery),
		Alias:           obj.Alias,
		BackgroundImage: obj.BackgroundImage,
		IsDefault:       obj.IsDefault,
		Status:          int32(obj.Status),
	}
}

func ToProtoGroups(objs []*aggregate.Group) []*pbdemo.Group {
	var rs []*pbdemo.Group
	for _, item := range objs {
		rs = append(rs, ToProtoGroup(item))
	}
	return rs
}
