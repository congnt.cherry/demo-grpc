IGNORE_DIR1=-path ./datastorage -prune -false -o
GOFMT_FILES?=$$(find . $(IGNORE_DIR1) -name '*.go' | grep -v vendor)
GOFMT := "goimports"

DEPEND=\
	golang.org/x/tools/cmd/goimports \
	golang.org/x/tools/cmd/stringer \
	github.com/swaggo/swag/cmd/swag \
	github.com/githubnemo/CompileDaemon \
	github.com/gogo/googleapis@v1.4.0 \
	github.com/gogo/protobuf@v1.3.2 \
	github.com/golang/protobuf@v1.4.3 \
	google.golang.org/grpc@v1.33.1 \
	github.com/gogo/protobuf/protoc-gen-gogo \
	github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway \
	github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger \
	github.com/mwitkow/go-proto-validators/protoc-gen-govalidators \
	github.com/matryer/moq

fmt: ## Run gofmt for all .go files
	@$(GOFMT) -w $(GOFMT_FILES)

lint: ## run linter
	docker run --rm -v $(shell pwd):/app -w /app golangci/golangci-lint:v1.33.0 golangci-lint run -v
mock:
	moq -out ./internal/repository/group/repository_mock.go ./internal/repository/group/ IRepository
dev:
	scripts/run_local.sh

depend: ## Install dependencies for dev
#	@go get -v ./...
	@go get -d $(DEPEND)
	@go mod vendor

generate: ## Generate proto
	protoc \
		-I /usr/local/include \
		-I /usr/local/share/googleapis \
		-I proto/ \
		-I vendor/ \
		--descriptor_set_out=./descriptors.protoset\
		--include_source_info --include_imports -I. \
		--gogo_out=plugins=grpc,\
Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types,\
Mgoogle/api/annotations.proto=github.com/gogo/googleapis/google/api,\
Mgoogle/protobuf/field_mask.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/struct.proto=github.com/gogo/protobuf/types:\
pb \
		proto/*.proto

help: ## Display this help screen
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
