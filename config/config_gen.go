//AUTO-GENERATED: DO NOT EDIT

package config

import (
	"strings"

	"demo/pkg/l"
	"github.com/spf13/viper"
)

var (
	ll = l.New()
)

// Tracing ...
type Tracing struct {
	Enabled bool
}

// Base ...
type Base struct {
	GRPCPort    int    `json:"grpc_port" mapstructure:"grpc_port"`
	Environment string `json:"environment" mapstructure:"environment"`
}

// Load ...
func Load(ll l.Logger, cPath ...string) *Config {
	var cfg = &Config{}

	viper.New()
	v := viper.NewWithOptions(viper.KeyDelimiter("__"))

	customConfigPath := "."
	if len(cPath) > 0 {
		customConfigPath = cPath[0]
	}

	v.SetConfigType("env")
	v.SetConfigFile(".env")
	if len(cPath) > 0 {
		v.SetConfigName(".env")
	}
	v.AddConfigPath(customConfigPath)
	v.AddConfigPath("/app")
	v.SetEnvKeyReplacer(strings.NewReplacer(".", "__"))
	v.AutomaticEnv()

	if err := v.ReadInConfig(); err != nil {
		ll.Info("Error reading config file", l.Error(err))
	}

	err := v.Unmarshal(&cfg)
	if err != nil {
		ll.Fatal("Failed to unmarshal config", l.Error(err))
	}

	ll.Debug("Config loaded", l.Object("config", cfg))
	return cfg
}
