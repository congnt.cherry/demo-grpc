// YOU CAN EDIT YOUR CUSTOM CONFIG HERE

package config

import "demo/pkg/mysql"

// Config ...
type Config struct {
	Base `mapstructure:",squash"`
	// Custom here

	Mysql *mysql.MySQL `json:"mysql" mapstructure:"mysql"`
}

//
//func (c *Config) GetHTTPAddress() string {
//	if _, err := strconv.Atoi(c.HTTPAddress); err == nil {
//		return fmt.Sprintf(":%v", c.HTTPAddress)
//	}
//	return c.HTTPAddress
//}
