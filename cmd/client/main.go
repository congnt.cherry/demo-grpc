package main

import (
	"context"
	"demo/config"
	pbdemo "demo/pb"
	"demo/pkg/container"
	"demo/pkg/l"
	"fmt"
	_ "github.com/gogo/protobuf/gogoproto"
	"github.com/gogo/protobuf/types"
	"google.golang.org/grpc"
	"time"
)

func main() {
	ll := l.New()
	//nolint:errcheck
	defer ll.Sync()
	container.Register(func() l.Logger {
		return ll
	})

	cfg := config.Load(ll)

	// grpc
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	conn, err := grpc.DialContext(ctx, fmt.Sprintf(":%v", cfg.GRPCPort),
		grpc.WithInsecure(),
	)

	if err != nil {
		ll.Fatal("Failed to dial server:", l.Error(err))
	}

	c := pbdemo.NewDemoClient(conn)
	group, err := c.NewGroup(context.Background(), &pbdemo.NewGroupRequest{
		Name:            "test",
		Description:     "description 1",
		Privacy:         pbdemo.Group_CLOSED,
		Discoverability: pbdemo.Group_HIDDEN,
		Cover:           "",
		Alias:           "test1",
		BackgroundImage: "",
		CreatorId:       "user1",
	})
	if err != nil {
		ll.Fatal("failed to create group", l.Error(err))
	}

	res, err := c.UpdateGroup(context.Background(), &pbdemo.UpdateGroupRequest{
		Id: group.Data.Id,
		Group: &pbdemo.UpdateGroupRequest_GroupRequest{
			Name:        "update name",
			Description: "Description",
			Privacy:     pbdemo.Group_PUBLIC,
		},
		UpdateMask: &types.FieldMask{
			Paths: []string{},
		},
	})
	ll.Info("Result", l.Object("res", res), l.Error(err))
}
