package main

import (
	"demo/config"

	"demo/pkg/container"
	handleossignal "demo/pkg/handle-os-signal"
	"demo/pkg/l"
)

func main() {
	ll := l.New()
	//nolint:errcheck
	defer ll.Sync()
	container.Register(func() l.Logger {
		return ll
	})

	cfg := config.Load(ll)

	// init os signal handle
	shutdown := handleossignal.New(ll)
	shutdown.HandleDefer(func() {
		ll.Sync()
	})
	container.Register(func() handleossignal.IShutdownHandler {
		return shutdown
	})

	go bootstrap(cfg)

	// handle signal
	if cfg.Environment == "D" {
		shutdown.SetTimeout(1)
	}
	shutdown.Handle()
}
