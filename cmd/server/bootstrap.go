package main

import (
	"demo/config"
	"demo/internal/app"
	"demo/internal/model/entity"
	"demo/internal/pkg"
	"demo/internal/repository/group"
	groupservice "demo/internal/service-group"
	groupstore "demo/internal/store/mysql/group"
	pbdemo "demo/pb"
	"demo/pkg/container"
	handleossignal "demo/pkg/handle-os-signal"
	"demo/pkg/l"
	"demo/pkg/mysql"
	"fmt"
	"net"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	"google.golang.org/grpc"
)

func bootstrap(cfg *config.Config) {
	var ll l.Logger
	container.Resolver(&ll)

	var shutdown handleossignal.IShutdownHandler
	container.Resolver(&shutdown)

	db := mysql.MustConnectDB(cfg.Mysql)
	container.Register(func() group.IRepository {
		return groupstore.NewGroupRepoImpl(db)
	})

	if cfg.Mysql.EnabledMigrate {
		ll.Info("Run AutoMigrate")
		db.AutoMigrate(&entity.Group{})
	}

	shutdown.HandleDefer(func() {
		db.Close()
	})

	container.Register(func() groupservice.IService {
		return groupservice.New()
	})

	// grpc server
	s := grpc.NewServer(
		grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
			pkg.LogUnaryServerInterceptor(ll),
		)),
	)

	svc := app.New()
	pbdemo.RegisterDemoServer(s, svc)

	ll.Info("GRPC server start listening", l.Int("GRPC address", cfg.GRPCPort))
	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", cfg.GRPCPort))
	if err != nil {
		ll.Fatal("error listening to address", l.Int("address", cfg.GRPCPort), l.Error(err))
		return
	}

	err = s.Serve(listener)
	if err != nil {
		ll.Fatal("")
	}
}
