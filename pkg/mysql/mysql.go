package mysql

import (
	"demo/pkg/l"
	"time"

	"github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

var ll = l.New()

// MySQL is settings of a MySQL server. It contains almost same fields as mysql.Config,
// but with some different field names and tags.
type MySQL struct {
	Username  string            `json:"username" mapstructure:"username"`
	Password  string            `json:"password" mapstructure:"password"`
	Protocol  string            `json:"protocol" mapstructure:"protocol"`
	Address   string            `json:"address" mapstructure:"address"`
	Database  string            `json:"database" mapstructure:"database"`
	Params    map[string]string `json:"params" mapstructure:"params"`
	Collation string            `json:"collation" mapstructure:"collation"`
	Loc       *time.Location    `json:"location" mapstructure:"loc"`
	TLSConfig string            `json:"tls_config" mapstructure:"tls_config"`

	Timeout      time.Duration `json:"timeout" mapstructure:"timeout"`
	ReadTimeout  time.Duration `json:"read_timeout" mapstructure:"read_timeout"`
	WriteTimeout time.Duration `json:"write_timeout" mapstructure:"write_timeout"`

	AllowAllFiles           bool   `json:"allow_all_files" mapstructure:"allow_all_files"`
	AllowCleartextPasswords bool   `json:"allow_cleartext_passwords" mapstructure:"allow_cleartext_passwords"`
	AllowOldPasswords       bool   `json:"allow_old_passwords" mapstructure:"allow_old_passwords"`
	ClientFoundRows         bool   `json:"client_found_rows" mapstructure:"client_found_rows"`
	ColumnsWithAlias        bool   `json:"columns_with_alias" mapstructure:"columns_with_alias"`
	InterpolateParams       bool   `json:"interpolate_params" mapstructure:"interpolate_params"`
	MultiStatements         bool   `json:"multi_statements" mapstructure:"multi_statements"`
	ParseTime               bool   `json:"parse_time" mapstructure:"parse_time"`
	GoogleAuthFile          string `json:"google_auth_file" mapstructure:"google_auth_file"`
	EnabledMigrate          bool   `json:"enabled_migrate" mapstructure:"enabled_migrate"`
	Debug                   bool
}

// FormatDSN returns MySQL DSN from settings.
func (m *MySQL) FormatDSN() string {
	um := &mysql.Config{
		User:                    m.Username,
		Passwd:                  m.Password,
		Net:                     m.Protocol,
		Addr:                    m.Address,
		DBName:                  m.Database,
		Params:                  m.Params,
		Collation:               m.Collation,
		Loc:                     time.Local,
		TLSConfig:               m.TLSConfig,
		Timeout:                 m.Timeout,
		ReadTimeout:             m.ReadTimeout,
		WriteTimeout:            m.WriteTimeout,
		AllowAllFiles:           m.AllowAllFiles,
		AllowCleartextPasswords: m.AllowCleartextPasswords,
		AllowOldPasswords:       m.AllowOldPasswords,
		ClientFoundRows:         m.ClientFoundRows,
		ColumnsWithAlias:        m.ColumnsWithAlias,
		InterpolateParams:       m.InterpolateParams,
		MultiStatements:         m.MultiStatements,
		ParseTime:               m.ParseTime,
		AllowNativePasswords:    true,
	}
	return um.FormatDSN()
}

// MySQLDSN returns the MySQL DSN from config.
func (m *MySQL) MySQLDSN() string {
	return m.FormatDSN()
}

// MustConnectDB ...
func MustConnectDB(cfg *MySQL) *gorm.DB {
	db, err := gorm.Open("mysql", cfg.MySQLDSN())
	if err != nil {
		panic(err)
	}

	err = db.Raw("SELECT 1").Error
	if err != nil {
		ll.Fatal("error querying SELECT 1", l.Error(err))
	}

	db.Callback().Create().Replace("gorm:update_time_stamp", func(scope *gorm.Scope) {
		_ = scope.SetColumn("CreatedAt", time.Now().Unix())
		_ = scope.SetColumn("UpdatedAt", time.Now().Unix())
	})
	db.Callback().Update().Replace("gorm:update_time_stamp", func(scope *gorm.Scope) {
		_ = scope.SetColumn("UpdatedAt", time.Now().Unix())
	})

	if cfg.Debug {
		// Enable Logger, show detailed log
		db.LogMode(true)
		db.SetLogger(NewGormLog())
	}

	return db
}
