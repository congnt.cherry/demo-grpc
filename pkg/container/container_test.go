package container

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

type Shape interface {
	SetArea(int)
	GetArea() int
}

type Circle struct {
	a int
}

func (c *Circle) SetArea(a int) {
	c.a = a
}

func (c Circle) GetArea() int {
	return c.a
}

func NewCircle(a int) Shape {
	return &Circle{a: a}
}

type Rectangle struct {
	a int
}

func (c *Rectangle) SetArea(a int) {
	c.a = a
}

func (c Rectangle) GetArea() int {
	return c.a
}

func TestSingleton(t *testing.T) {
	area := 5

	Register(func() Shape {
		return NewCircle(area)
	})
	Register(func() Shape {
		return &Rectangle{a: area}
	})

	UseInOtherPlace(t, area)

	var myRectangle Shape
	Resolver(&myRectangle)
	a := myRectangle.GetArea()
	assert.Equal(t, area, a)
}

func UseInOtherPlace(t *testing.T, area int) {
	var myCircle Shape
	Resolver(&myCircle)
	a := myCircle.GetArea()
	assert.Equal(t, area, a)
}
