FROM golang:1.16-alpine as builder
RUN apk add --no-cache --update bash git

WORKDIR /src

ADD ./go.mod ./go.sum ./
RUN go mod download

ADD ./ ./

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o /dist/server cmd/server/*.go

FROM alpine:latest

RUN apk add --update ca-certificates && \
    rm -rf /var/cache/apk/*

COPY --from=builder /dist/server /app/bin/server
COPY --from=builder /src/.env.example /app/bin/.env

WORKDIR /app/bin
EXPOSE 1000


