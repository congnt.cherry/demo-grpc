# Demo use GRPC with simple CRUD + MySQL

## Requirement
- Go
- MySQL
- Protoc

## Run dev with docker

config environment variables in [.env](.env)

Note: set env `MYSQL__ENABLED_MIGRATE=true` to auto create table

```bash
docker-compose up dev
```

## GRPC Client

I have an example grpc client in `cmd/client` folder